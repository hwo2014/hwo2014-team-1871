
using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace helloworld
{


	public class CarPosition
	{
		public CarId id;
		public double angle;
		public PiecePosition piecePosition;

	}

	public class Position
	{
		public JObject id;
		public double angle;
		public PiecePosition piecePosition;

	}

	public class PiecePosition
	{
		public int pieceIndex;
		public double inPieceDistance;
		public PositionLane lane;
		public int lap;
	}

	public class PositionLane
	{
		public int startLaneIndex;
		public int endLaneIndex;

	}

	public class Lane
	{
		public int distanceFromCenter;
		public int index;
	}

	public class RaceWrapper
	{
		public Race race;
	}

	public class Race
	{
		public Track track;
		public Car[] cars;
		public RaceSession raceSession;

	}

	public class Car
	{
		public CarId id;
		public Dimension dimensions;
	}

	public class CarId
	{
		public string name;
		public string color;
	}



	public class RaceSession
	{
		public int laps;
		public int maxLapTimeMs;
		public bool quickRace;
	}

	public class Dimension
	{
		public double length;
		public double width;
		public double guideFlagPosition;
	}

	public class Track
	{
		public string id;
		public string name;
		public Piece[] pieces;
		public Lane[] lanes;
		public StartingPoint startingPoint;
	}

	public class StartingPoint
	{
		public StartPosition position;
		public double angle;
	}

	public class StartPosition
	{
		public double x;
		public double y;
	}

	public class Piece
	{
		public double length;
		public bool @switch;
		public int radius;
		public int angle;
	}


	public class CreateRace
	{
		public JObject botId;
		public string trackName;
		public string password;
		public int carCount;
	}

    public class Crash
    {
        public string name;
        public string color;

    }
	public class BotId
	{
		public string name;
		public string key;
		public BotId(string name, string key)
		{
			this.name = name;
			this.key = key;
		}
	}

	public class TurboAvailable
	{
		// TODO 2 
		public double turboDurationMilliseconds;
		public int turboDurationTicks;
		public double turboFactor;

	}

	public class SwitchPiece
	{
		public int index;
		public int numberOfLanes;

		public SwitchPiece(int index, int numLanes)
		{
			this.index = index;
			this.numberOfLanes = numLanes;
		}
	}


}