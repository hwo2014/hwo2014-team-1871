using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;


namespace helloworld
{
	public class Bot {
		public static void Main(string[] args) {
		    string host = args[0];
	        int port = int.Parse(args[1]);
	        string botName = args[2];
	        string botKey = args[3];

			myCarKey = botKey;
			myCarName = botName;
			Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

			using(TcpClient client = new TcpClient(host, port)) {
				NetworkStream stream = client.GetStream();
				StreamReader reader = new StreamReader(stream);
				StreamWriter writer = new StreamWriter(stream);
				writer.AutoFlush = true;

				// TODO 1 - change to join for check in
                new Bot(reader, writer, new Join(botName, botKey)); //TODO 2: Set dynamically change track
                //new Bot(reader, writer, new JoinRace(botName, botKey, "imola", 1));
                
                //new Bot(reader, writer, new JoinRace(botName, botKey, "elaeintarha", 2, "smipassword"));
                //new Bot(reader, writer, new CreateNewRace(botName, botKey, "imola", 4));
			}
		}

		private StreamWriter writer;
		private TrackAnalysis analysis;
		private SpeedCalculation sp;
		private SwitchAnalysis sw;
		private static string myCarKey;
		private static string myCarName;
		private CarId myCarId;
		private int stabilizeCounter;
        private OvertakeLogic overtake;
		//private Physics physics;

		Bot(StreamReader reader, StreamWriter writer, SendMsg join) {
			try
			{
				stabilizeCounter = 4;
				analysis = new TrackAnalysis();
				sp = new SpeedCalculation(); // TODO 0.4 Pass the Sp to car analysis
				this.writer = writer;
				string line;
				//bool tempOnce = false;
				//bool tempOnce2 = false;
				double maxAngle = 0;
				int maxAngleAt = 0;
				bool turboAvailable = false;
				bool initOnce =  false;
				analysis.trackTurns  = new TrackTurns();
				sw = new SwitchAnalysis();
				var physics = Physics.GetInstance();
                overtake = new OvertakeLogic(myCarName);
                int switchWaitCounter = 0;
				send(join);

				while((line = reader.ReadLine()) != null) 
				{
					MsgWrapperRecv msg = JsonConvert.DeserializeObject<MsgWrapperRecv>(line);
					if(msg == null)
					{
						Console.WriteLine("Empty message received");
						continue;
					}
				

					switch(msg.msgType) 
					{
						case "carPositions":
							{
                                try
                                {
                                    JArray jobj = msg.data as JArray;
                                    double setThrottle = 0.7;
                                    var switchDir = string.Empty;
                                    stabilizeCounter--;
                                    if (jobj != null)
                                    {
                                        List<CarPosition> cars = jobj.ToObject<List<CarPosition>>();

                                        if (cars != null)
                                        {
                                            var carPos = cars.First(cp => cp.id.name == myCarName);
                                            var carIndex = carPos.piecePosition.pieceIndex;
                                            Console.WriteLine(msg.gameTick);
                                            overtake.UpdateState(cars);

                                            var hasJustSwitchedLane = sw.CheckSwitchedLane(carPos);
                                            // TODO - Readjust distance before switch!


                                            var curSpeed = sp.UpdatePosition(carPos);
                                            physics.Learn(curSpeed);
                                            // lap learning
                                            analysis.trackTurns.RecordAngle(carPos, curSpeed); // TODO: Adjust per speed/throttle?
                                            analysis.RunLapAnalysis(carPos);
                                            analysis.UpdateCarIndex(carPos.piecePosition.pieceIndex);

                                            if (turboAvailable &&
                                                analysis.trackTurns.IsSafeForTurbo(carPos.piecePosition.pieceIndex,
                                                    RaceInfo.GetInstance().raceData.turboAvailable.turboFactor,
                                                    RaceInfo.GetInstance().raceData.turboAvailable.turboDurationTicks,
                                                    carPos.angle,
                                                    curSpeed))
                                            {
                                                turboAvailable = false;
                                                send(new Turbo());
                                            }

                                            setThrottle = analysis.SuggestThrottle(
                                                carPos.piecePosition.pieceIndex,
                                                carPos.piecePosition.inPieceDistance,
                                                curSpeed,
                                                carPos.angle);

                                            //RaceInfo.GetInstance().raceData.racelog.Log(msg.gameTick, cars [0].piecePosition.pieceIndex, curSpeed, 0, cars[0].angle, setThrottle);
                                            //Console.WriteLine("distance: " + carPos.piecePosition.inPieceDistance);
                                            Console.Write("index: " + carPos.piecePosition.pieceIndex);
                                            Console.Write(" angle: " + carPos.angle);
                                            Console.Write(" speed: " + curSpeed);
                                            Console.WriteLine(" throttle: " + setThrottle);

                                            if (maxAngle < Math.Abs(carPos.angle))
                                            {
                                                maxAngle = Math.Abs(carPos.angle);
                                                maxAngleAt = carPos.piecePosition.pieceIndex;
                                            }
                                            if (sw.SwitchPending == true)
                                            {
                                                // If we're waiting too long - reset
                                                if (switchWaitCounter > 14)
                                                {
                                                    switchWaitCounter = 0;
                                                    sw.SwitchPending = false;
                                                    sw.SwitchSent = false;
                                                }
                                                switchWaitCounter++;
                                            }
                                            if ((sw.SwitchPending == false) && stabilizeCounter < 0
                                                && sw.NearSwitch(carPos.piecePosition.pieceIndex))
                                            {
                                                // See if need to switch
                                                if (overtake.DetermineSwitch())
                                                {
                                                    if (switchDir != string.Empty)
                                                    {
                                                        Console.WriteLine("Switch to other route");
                                                        var ran = new System.Random();
                                                        var randResult = ran.Next(4);
                                                        switch (randResult)
                                                        {
                                                            case 0:
                                                                switchDir = sw.GetSwitchToOtherRoute(carPos.piecePosition.pieceIndex,
                                                                RaceInfo.GetInstance().raceData.currentActiveLane);
                                                                break;
                                                            case 1:
                                                                switchDir = "Left"; break;
                                                            case 2:
                                                                switchDir = "Right"; break;
                                                            default:
                                                                switchDir = string.Empty; break;
                                                        }


                                                        if (switchDir != string.Empty)
                                                        {
                                                            sw.SwitchPending = true;
                                                            sw.SwitchSent = false;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    switchDir = sw.GetSwitchToShortestRoute(carPos.piecePosition.pieceIndex,
                                                        RaceInfo.GetInstance().raceData.currentActiveLane);
                                                    if (switchDir != string.Empty)
                                                    {
                                                        sw.SwitchPending = true;
                                                        sw.SwitchSent = false;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    // Should we switch?
                                    if (switchDir != string.Empty)
                                    {
                                        Console.WriteLine("Switch command: " + switchDir);
                                        send(new SwitchLane(switchDir)); // TODO 3 what to do if we only have one switch command possible
                                        sw.SwitchSent = true;
                                        switchDir = string.Empty;
                                    }
                                    else
                                    {
                                        send(new Throttle(setThrottle));
                                    }
                                    
                                }
                                catch(Exception ex)
                                {
                                    Console.WriteLine(ex);
                                    send(new Throttle(0.5));
                                }

                                break;
							}
						case "join":
							Console.WriteLine("Joined");
							send(new Ping());
							break;
						case "gameInit":
							{
                                JObject jobj = msg.data as JObject;
                                RaceWrapper createdRace = jobj.ToObject<RaceWrapper>();
								if(initOnce == true)
								{
									Console.WriteLine("Second game init called");
									// Reset stuff
									sw.SwitchPending = false;
									sw.SwitchSent = false;
									stabilizeCounter = 4;

                                    if (createdRace != null)
                                    {
                                        var lanes = createdRace.race.track.lanes;
                                        RaceInfo raceInfo = RaceInfo.GetInstance();
                                        raceInfo.race.track.lanes = lanes;
                                        analysis.trackTurns.ResetMaxSpeed();
                                    }
									send(new Ping());
									break;
								}
								else
									initOnce = true;
                                								
								Console.WriteLine(msg.data);
								if(jobj == null)
										Console.WriteLine("game init data cast to jobj is null");
								
                                							
								if(createdRace != null)
								{
									analysis.pieces = new Dictionary<int, Piece>();
									for(int i=0; i<createdRace.race.track.pieces.Count(); i++)
									{
										analysis.pieces.Add(i, createdRace.race.track.pieces[i]);
									}

									Console.WriteLine(createdRace.race.raceSession);
									RaceInfo raceInfo = RaceInfo.GetInstance();
									raceInfo.SafeRace(createdRace.race);

									analysis.trackTurns.LearnTrack(createdRace.race.track.pieces);
									sw.Analyse(createdRace.race.track.pieces);
								}

							
								Console.WriteLine ("Race init");
								send (new Ping ());
								break;
							}
						case "gameEnd":
							RaceInfo.GetInstance().raceData.racelog.Print();
							Console.WriteLine("Race ended");
							send(new Ping());
							Console.WriteLine("Max Angle:" + maxAngle + ", " + maxAngleAt);

							break;
						case "gameStart":
							Console.WriteLine("Race starts");
							send(new Ping());
							break;
						case "dnf":
							Console.WriteLine("disqualified");
							break;
						case "turboAvailable":
							{
								Console.WriteLine("TurboAvailable <<<<<<<< ");

								JObject jobj = msg.data as JObject;
								if(jobj == null)
									Console.WriteLine("game init data cast to jobj is null");


								TurboAvailable turboAvail = jobj.ToObject<TurboAvailable>();
								RaceInfo.GetInstance().raceData.turboAvailable = turboAvail;

								Console.WriteLine("Turbo ticks=" + turboAvail.turboDurationTicks + " turboFactor" + turboAvail.turboFactor);
								send(new Ping());
								turboAvailable = true;
								break;
							}
                    case "crash":
                            {
                                Console.WriteLine("Just crashed");

                                JObject jobj = msg.data as JObject;
                                if(jobj == null)
                                    Console.WriteLine("game init data cast to jobj is null");


                                Crash crash = jobj.ToObject<Crash>();

                                if(crash.name == myCarName)
                                {
                                    analysis.JustCrashed();
                                    sw.SwitchPending = false;
                                    sw.SwitchSent = false;
                                }
                                Console.WriteLine(msg.gameTick);

                                send(new Ping());

                                break;
                            }
					default: // TODO 3: Handle tournament end or game End
							Console.WriteLine(msg.msgType);
							send(new Ping());
							break;
					}
				}
			}
			catch(Exception ex) {
				Console.WriteLine (ex);
			}
		}

		private void send(SendMsg msg) {
			writer.WriteLine(msg.ToJson());
		}

		private void UpdateLane(CarPosition carPos)
		{

		}

		private CarId ExtractCarId(IEnumerable<Car> cars, string carName)
		{
			var car = cars.FirstOrDefault(c => c.id.name == carName);
			if (car == null)
			{
				Console.WriteLine("Error: Unable to find car id on name: " + carName);
			}

			return car.id;
		}

		private void AnalyseLap()
		{

		}
	}



	// TODO 3 rename to a better name
	public class SpeedCalculation
	{
		CarPosition lastPos;
		CarPosition newPos;
		double lastSpeed;

		public SpeedCalculation()
		{
			lastPos = new CarPosition ();
			newPos = new CarPosition ();
			lastSpeed = 0;
		}

		public double UpdatePosition(CarPosition newPosition)
		{
			lastPos = newPos;
			newPos = newPosition;

			var dist = CalculateDistance ();
            if (dist == 0.0)
                return 0.0;
            else if (dist < 0.0)
            {
                return lastSpeed;
            }
            else if (dist < lastSpeed - 1.0 && dist > 0.2) // likely an error due to switches
            {
                return lastSpeed;
            }
            else if(dist > lastSpeed + 1.0 && dist > 0.2) // likely error  due to switches
            {
                return lastSpeed;
            }
            else
			{
				lastSpeed = dist;
				return lastSpeed;
			}
		}

		public double CalculateDistance ()
		{
			double distance = 0.0;
			if (lastPos.piecePosition == null || newPos.piecePosition == null)
				return 0.0;

			if (newPos.piecePosition.pieceIndex == lastPos.piecePosition.pieceIndex)
			{
				distance = newPos.piecePosition.inPieceDistance - lastPos.piecePosition.inPieceDistance;
			}
			else
			{
				// Get current piece info
				var raceInfo = RaceInfo.GetInstance();
				var lastPiece = raceInfo.race.track.pieces[lastPos.piecePosition.pieceIndex];
				var newPiece = raceInfo.race.track.pieces[newPos.piecePosition.pieceIndex];

				var lastPieceDistance = lastPiece.RemainingDistance(lastPos.piecePosition.inPieceDistance);
				var newPieceDistance = newPos.piecePosition.inPieceDistance;
				distance = lastPieceDistance + newPieceDistance;
			}

			return distance;
		}

		public double CalculateSpeed(double distance, int ticks)
		{
			return distance / ticks;
		}
	}


	#region
	// 2. Advance lookup for switch
	// 3. Speed analysis, Distance prediction
	// 3. Crash avoidance
	// 4. Lap learning
	// 5. Exit acceleration
	// 6. Current Lap Learning
	// 7. Race mode - single or compete
	// 8. Lane consideration for risk and throttle
	// 9. Fine tune risk factor/throttle
	// 10. Fine tune weightage factor/throttle
	#endregion


	public class RaceInfo
	{
		private static RaceInfo instance;
		public Race race;
		public RaceRunData raceData;

		private RaceInfo()
		{
			raceData = new RaceRunData();
			raceData.currentActiveLane = 0;

		}

		public Track track
		{ 
			get
			{
				return race.track;
			}
		}

		public static RaceInfo GetInstance()
		{
			if (instance == null)
			{
				instance = new RaceInfo();
			}

			return instance;
		}

		public void SafeRace(Race race)
		{
			this.race = race;
		}
	}

	public class RaceRunData
	{
		public double maximumAngle;
		public int currentActiveLane = 1;
		public int nextActiveLane = 1  ;
		public RaceLog racelog;
		public CarPosition lastPosition;
		public TurboAvailable turboAvailable;

		public RaceRunData()
		{
			racelog = new RaceLog();
			lastPosition = new CarPosition();
		}
	}

	public class RaceLog
	{
		public Dictionary<int, string> log;

		public RaceLog()
		{
			log = new Dictionary<int, string>();
		}

		public void Log(int gametick, double inPiecePos, double speed, double accel, double angle, double throttle)
		{
			var key = gametick;
			if(log.ContainsKey(gametick))
				key = gametick++;
			log.Add(key, ":" + inPiecePos + ", " + speed + ", " + accel + ", " + angle + ", " + throttle);
		}

		public void Print()
		{
//			foreach (var kv in log)
//			{
//				Console.WriteLine(kv.Key + kv.Value);
//			}
		}
	}

	public static class PieceExtension
	{
		public static bool IsCurved(this Piece piece)
		{
			if (piece.angle != 0)
				return true;

			return false;
		}

		public static double GetCurveLevel(this Piece piece, int laneNumber)
		{
			double level = piece.angle / 10;
			if (level < 0)
				level *= -1;

			//adjust radius
			if (piece.angle != 0)
			{
				if (piece.radius <= 50)
				{
					level *= 1.7;
				}
				else if (piece.radius >= 101)
				{
					level *= 0.9;
				}
			}
			return level;
		}

		public static double RemainingDistance(this Piece piece, double elapsedDistance)
		{
			// Current lane?
			var laneActive = RaceInfo.GetInstance().raceData.currentActiveLane;
				
			return piece.RemainingDistance(elapsedDistance, laneActive);

		}

		public static double RemainingDistance(this Piece piece, double elapsedDistance, int lane)
		{
			if (piece.angle == 0)
			{
				return piece.length - elapsedDistance;
			}
			else
			{
				var pieceDistance = piece.LaneLength(lane);
				return pieceDistance - elapsedDistance;
			}
		}


		public static double LaneLength(this Piece piece, int laneIndex)
		{
			if (piece.length != 0)
				return piece.length;


			var distanceOfLane = RaceInfo.GetInstance().race.track.lanes[laneIndex].distanceFromCenter;
			//Console.WriteLine("Distance of lane =" + distanceOfLane);

			// Depends on curvature of piece
			// Assumption
			// lane 0 having positive curve angle will be on outer side
			// lane 1 (or more) have positive curve angle will be on inner side
			distanceOfLane *= (piece.angle > 0) ? 1 : -1;
			//Console.WriteLine("New curve distance of lane =" + distanceOfLane);

			var distance = 2 * Math.PI * (piece.radius - distanceOfLane) * Math.Abs(piece.angle) / 360;
			//Console.WriteLine("Curve Distance result is " + distance);
			return distance;
		}
	}


	public class TrackAnalysis
	{
		public Dictionary<int, Piece> pieces;

		const double AngleLimit = 45.0;
		double AngleSwing = 0;
		double LastAngle = 0;
		public TrackTurns trackTurns;
		private int lastIndex;
		private bool pauseLapAnalysis;

		public TrackAnalysis()
		{
			lastIndex = 0;
			pauseLapAnalysis = false;

		}

        public void JustCrashed()
        {
            Console.WriteLine("Crash at index: " + lastIndex);
            trackTurns.JustCrashed(lastIndex);


        }
		// TODO 2: Add lane
		public double SuggestThrottle(int currentCarTrackPos, double posOffset, double currentSpeed, double currentAngle)
		{
			AngleSwing = Math.Abs(currentAngle) - Math.Abs(LastAngle);
			LastAngle = currentAngle;

			try
			{
				double t = 0.0;
				t = trackTurns.IsSafeToNextTurn(currentSpeed, currentCarTrackPos, posOffset, AngleSwing, currentAngle);
				//	Console.WriteLine("Throttle is set to: " + t);
				if(t <= 1.0 && t >= 0.0)
				{
					return t;
				}
				return 1.0;
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex);
			}
            return 0.5;

		}

        public void UpdateCarIndex(int index)
        {
            lastIndex = index;
        }

		public void RunLapAnalysis(CarPosition carPos)
		{

			if (lastIndex != carPos.piecePosition.pieceIndex)
			{
				lastIndex = carPos.piecePosition.pieceIndex;
				if (lastIndex == 0)
					pauseLapAnalysis = false;
				var lastIndexInTrack = RaceInfo.GetInstance().race.track.pieces.Count()-1;

				if (pauseLapAnalysis == false && lastIndex == lastIndexInTrack)
				{
					Console.WriteLine("Analysing Lap now");
					pauseLapAnalysis = true;
					trackTurns.AnalyseLap();
				}
		
			}
		}
	}

	public class Turn
	{
		public double curvature;
		public double radius;
		public Dictionary<int, int> laneOffset;
		public int countPiece;
		public int startIndex;
		public int endIndex;
		public double learningMultiplierMaxSpeed;
        public double prevMultiplierMaxSpeed;
        public int learnCount;
        public int masterAdjustmentCount;
        public double masterAdjustment;
        public double prevMasteradjustment;
        public double nextTurnEffect;
        private Turn nextTurn;
        private bool justCrashed;
        private List<double> MaxAngleHistory;
        private bool increaseCap;
		//double maxSpeed;
		Dictionary<int, double> maxSpeedOnLane;
		// TODO 3: consider more than 3 lanes

		double lastAngle;
		List<double> MaxAngleList;
		private Slippage slippage;

		public Turn()
		{
			laneOffset = new Dictionary<int, int>();
			maxSpeedOnLane = new Dictionary<int, double>();
			var lanes = RaceInfo.GetInstance().race.track.lanes;
			MaxAngleList = new List<double>();
			learningMultiplierMaxSpeed = 1.0;
            prevMultiplierMaxSpeed = 1.0;
			slippage = new Slippage();
            masterAdjustment = 0.0;
            prevMasteradjustment = 0.0;
            masterAdjustmentCount = 0;
            nextTurnEffect = 1.0;
            justCrashed = false;
            MaxAngleHistory = new List<double>();
            increaseCap = false;
			foreach (Lane l in lanes)
			{
				laneOffset.Add(l.index, l.distanceFromCenter);
			}

            learnCount = 0;
	
		}

        public void SetNextTurn(Turn nextTurn)
        {
            this.nextTurn = nextTurn;
        }

        public void ResetSpeed()
        {
            maxSpeedOnLane.Clear();

        }
		public double MaxDeceleration(double curSpeed)
		{

			var phy = Physics.GetInstance();
			return phy.friction * curSpeed / phy.mass;

			// finland
			//return 0.1 * curSpeed / 4.9;

			// germany
			//return 0.06 * curSpeed / 3.0;

			// TODO: Derive value from the track
		}

		public double SafeDistanceForBreak(double curSpeed)
		{
			var lane = RaceInfo.GetInstance().raceData.currentActiveLane;
			return SafeDistanceForBreak(curSpeed, lane);
		}

		public double SafeDistanceForBreak(double curSpeed, int lane)
		{
			// find speed difference
            var maxSpeed = GetMaxSpeed(lane, null); // todo 1 LANE CHANGE
			Console.WriteLine("The max speed is " + maxSpeed + " Lane=" + lane);

			var difference = curSpeed - maxSpeed;
			if (difference < 0) // we're slower
				return 0;

			// Regression method will be expensive. Approximate linearly
			var midSpeed = (difference + maxSpeed) / 2;
			var decel = MaxDeceleration(midSpeed); // deceleration at middle speed
			var tickRequired = difference / decel;

			var errorFromLongTick = 0.0;
            errorFromLongTick = Math.Pow((tickRequired) / 16, 2) * 0.053 + 1; // Assume - 5 percent error when tick is higher than 20
			if (errorFromLongTick > 1.05)
				errorFromLongTick = 1.05;

			var distance = tickRequired * midSpeed / errorFromLongTick; // linearApproximation with correction

			//Console.WriteLine("Safe Distance= " + distance + ", tick=" +tickRequired);
			return distance;
		}

        public double GetMaxSpeed(int lane, Turn nextTurn)
		{
			var speed = 0.0;
            if (nextTurn != null)
                ResetSpeed();

			if( maxSpeedOnLane.TryGetValue(lane, out speed) == false)
			{
                speed = CalculateMaxSpeed(lane, nextTurn);
				maxSpeedOnLane.Add(lane, speed);
			}

			return speed;

		}

        private double CalculateMaxSpeed(int lane, Turn nextTurn)
		{
			// at Rad = 100. At normal mass and friction
            double baseSpeed = 6.7; // calculated at 6.82 

			double laneSpeed;
			if(this.curvature > 0) // TODO - Switch curvature?
				laneSpeed = (Math.Sqrt((radius - laneOffset[lane])/100)) * baseSpeed;
			else
				laneSpeed = (Math.Sqrt((radius + laneOffset[lane])/100)) * baseSpeed;

			//Console.WriteLine("Initial laneSpeed = " + laneSpeed);

			// safety factor - don't know why speed lower when R is smaller
			// Could be because of mass lighter? 
			if (radius <= 50) // Todo 1- adjust linearly
                laneSpeed *= 1.007;
			else
				laneSpeed *= 1.0; // germany map 1.22

			// We still have safety drifting possibility. 
			if (Math.Abs(curvature) < 46)
			{
                laneSpeed *= 1.028; // TODO 3 trial and error
			}else if(Math.Abs(curvature)  < 23)
			{
                laneSpeed *= 1.10; // TODO 3 trial and error
			}

            var nextTurnEffect = 1.0;
            var nextTurnEffectAbs = 0.0;
            if (nextTurn != null)
            {
                // check distance first
                var diffIndex = nextTurn.startIndex - this.endIndex;
                if(diffIndex > 0 && diffIndex < 2)
                {
                    if (nextTurn.curvature * this.curvature > 0) // same side
                    {
                        if (nextTurn.radius <= this.radius) // this is dangerous
                        {
                            nextTurnEffect = 0.97;
                            nextTurnEffectAbs = -0.22;
                            if (nextTurn.GetMaxSpeed(lane, null) < laneSpeed * learningMultiplierMaxSpeed + masterAdjustment)
                                nextTurnEffectAbs -= 0.4;
                        }
                    }
                    else
                    {
                        if (nextTurn.radius > this.radius) // this is safe
                        {
                            nextTurnEffect = 1.03;
                            if (Math.Abs(nextTurn.curvature) < 91 && Math.Abs(this.curvature) < 91)
                                nextTurnEffectAbs = 0.14;
                        }
                        else
                        {
                            if (Math.Abs(nextTurn.curvature) < 23 && Math.Abs(this.curvature) < 23)
                            {
                                nextTurnEffect = 1.018;
                                nextTurnEffectAbs = 0.14;
                            }
                            else
                            if (Math.Abs(nextTurn.curvature) < 91 && Math.Abs(this.curvature) < 91)
                            {
                                nextTurnEffect = 0.99;
                                nextTurnEffectAbs = -0.2;
                            }


                        }
                    }

                }
            }

            return laneSpeed  * learningMultiplierMaxSpeed * nextTurnEffect + masterAdjustment + nextTurnEffectAbs;

		}

		public double GetPercentageCompletion(int index, double inPieceDistance)
		{
			var currentIndex = index;
			var distanceElapsedOnCurrentPiece = inPieceDistance;
			var completedPieceElapsed = 0.0;

			var sumDistance = 0.0;
			var lane = RaceInfo.GetInstance().raceData.currentActiveLane;

			for (int i = this.startIndex; i <= this.endIndex; i++)
			{
				sumDistance += RaceInfo.GetInstance().race.track.pieces[i].LaneLength(lane);
			}

			var completedIndexEnd = currentIndex;
			if (completedIndexEnd <= this.startIndex)
				completedPieceElapsed = 0;
			else
			{
				for (int j = this.startIndex; j < completedIndexEnd; j++)
				{
					completedPieceElapsed += RaceInfo.GetInstance().race.track.pieces[j].LaneLength(lane);
				}
			}

			return (completedPieceElapsed + distanceElapsedOnCurrentPiece) / sumDistance * 100;
		}

		public void RecordMaxAngle(int index, double angle, double speed)
		{
			//safety check
			if (index > endIndex || index < startIndex)
				return;

            var maxSpeed = GetMaxSpeed(RaceInfo.GetInstance().raceData.currentActiveLane, null);

			slippage.Record(angle, speed, maxSpeed);

			if(speed  >= 0.93 * maxSpeed)
				StoreMax(angle); // Only consider max angle when we're close to max speed

		}

		private void StoreMax(double angle)
		{
			var minAngle = 0.0;
			if(MaxAngleList.Count() > 0)
				minAngle = MaxAngleList.Min();
			if (Math.Abs(angle) > minAngle)
			{
				if (MaxAngleList.Count() > 2)
				{
					MaxAngleList.Remove(minAngle);
					MaxAngleList.Add(Math.Abs(angle));
				}
				else
					MaxAngleList.Add(Math.Abs(angle));
			}
		}

        public void JustCrashed()
		{
            if (prevMasteradjustment == 0.0)
            {
                if (masterAdjustment > 0.15)
                    masterAdjustment -= 0.15;
                else
                    masterAdjustment = 0;
            }
            else
            {
                masterAdjustment = prevMasteradjustment;
                prevMasteradjustment = 0.0;
            }

            if (prevMultiplierMaxSpeed == 1.0)
            {
                learningMultiplierMaxSpeed *= 0.98;
            }
            else
            {
                learningMultiplierMaxSpeed = prevMultiplierMaxSpeed;
                prevMultiplierMaxSpeed = 1.0;
            }

            learnCount-=2;
            MaxAngleList.Clear();
            ResetSpeed();
            justCrashed = true;
            Console.WriteLine("Crash adjustment. LearnMultiplier " + learningMultiplierMaxSpeed + " masterAdjustment " + masterAdjustment);
		}

        public void JustCrashedReduceGlobal()
        {
            if (learnCount < 2)
            {
                learningMultiplierMaxSpeed *= 0.97;
                if (masterAdjustment > 0.087)
                    masterAdjustment -= 0.087;
                else
                    masterAdjustment -= 0.07;
            }
            else
            {
                learningMultiplierMaxSpeed *= 0.995;
                if (masterAdjustment > 0.06)
                    masterAdjustment -= 0.05;
                else
                    masterAdjustment -= 0.035;
            }


            learningMultiplierMaxSpeed *= 0.993;
            if (masterAdjustment > 0.075)
                masterAdjustment -= 0.075;
            else
                masterAdjustment -= 0.06;

            ResetSpeed();
            Console.WriteLine("Global Crash adjustment. LearnMultiplier " + learningMultiplierMaxSpeed + " masterAdjustment " + masterAdjustment);

        }

		public double GetMaxAngles()
		{
			if (MaxAngleList.Count() == 0)
				return 0.0;

			var sum = 0.0;
			foreach (double d in MaxAngleList)
			{
				sum += d;
			}

			return sum / MaxAngleList.Count();
		}

        private void UpdateMaxAngleHistory(double maxAngle)
        {
            if (MaxAngleHistory.Count > 2)
            {
                MaxAngleHistory.RemoveAt(0);
            }

            MaxAngleHistory.Add(maxAngle);
        }

		public void AnalyseMaxAngle()
		{
            var max = GetMaxAngles();
            learnCount++;

            if (justCrashed == true)
            {
                justCrashed = false; 
                UpdateMaxAngleHistory(60.0);
                return;
            }
            else
            {
                UpdateMaxAngleHistory(max);
                prevMasteradjustment = masterAdjustment;
                prevMultiplierMaxSpeed = learningMultiplierMaxSpeed;
            }
			
			
			if (max > 58.0)
                learningMultiplierMaxSpeed *= 0.98;

            // Check on next turn status
            var nextTurn = FindNextTurn(this.endIndex);
            var nextTurnDiffIndex = 10;
            var cap = 1.058;
            if (nextTurn != null)
            {
                // distance
                nextTurnDiffIndex = nextTurn.startIndex - this.endIndex;
                if (nextTurnDiffIndex == 1)
                {
                    if (nextTurn.curvature * this.curvature > 0 &&
                        nextTurn.radius < this.radius)
                    {
                        cap = 1.016;
                    }
                    else if (nextTurn.curvature * this.curvature < 0 )
                    {
                        cap = 1.065;
                    }
                }
            }

            if(MaxAngleHistory.Average()< 45.0 && MaxAngleHistory.Count > 1 && learnCount >= 3)
            {
                Console.WriteLine("Increase cap due to low maxangle");
                increaseCap = true;
            }

            var lessIncrement = 1.0;
            var nextTurnMaxAngle = 0.0;
            if (nextTurn != null && nextTurnDiffIndex <= 2)
            {
                nextTurnMaxAngle = nextTurn.GetMaxAngles();
                if (nextTurnMaxAngle > 52.0)
                    lessIncrement = 0.4;
                else if(nextTurnMaxAngle < 35.0)
                {
                    if(increaseCap)
                        cap *= 1.055;
                }
                else if(nextTurnMaxAngle >= 40.0)
                {
                    lessIncrement *= 1.0 - (nextTurnMaxAngle - 40)/12.0*0.4;      
                }
            }

            if (max > 52.7)
				return;

            if (learnCount > 6)
				return; // cap at 6 times

            
            if (learnCount > 2)
            {
                lessIncrement *= Math.Pow(0.88, learnCount-2);
            }

			if (max > 45)
			{
                learningMultiplierMaxSpeed *= 1 + 0.006 * lessIncrement * (52.7 - max)/7.0;
			}
			else if (max > 37)
			{
                learningMultiplierMaxSpeed *= 1 + 0.008* lessIncrement;
			}
			else if (max > 20)
			{
                learningMultiplierMaxSpeed *= 1 + 0.012 * lessIncrement;
			}
			else if (max < 0.3)
			{
				learningMultiplierMaxSpeed *= 1.0;
			}
			else
			{
                learningMultiplierMaxSpeed *= 1 + 0.019 * lessIncrement;
			}

            if (learningMultiplierMaxSpeed > cap )
                learningMultiplierMaxSpeed = cap; // cap the increase - just in case


			Console.WriteLine("Lap Analysis. Adjusted max speed by: " + learningMultiplierMaxSpeed);
			maxSpeedOnLane.Clear();
            learnCount++;
		}

        public double AnalyseSlippage()
		{
			var res = slippage.AnalyseSlippage(3, 8);
			var init = slippage.InitialSwing();
			var spPercent = slippage.AverageSpeedPercentage(2,6);
			var qualified = slippage.IsQualified();
			var turn = this.curvature;

			//Console.WriteLine("Analyse slippage per turn");
			Console.WriteLine("Index " + startIndex + " " + res + " " + slippage.entryAngle + " " + init + " "+ spPercent + " " + qualified + " " + turn);

            return res;
		}

        public bool SlippageQualified()
        {
            return slippage.IsQualified();
        }

        public void ResetSlippage()
        {
            slippage.Reset();
        }

        public void ApplyMasterAdjustment(double adjust)
        {

            // Check for danger
            var nextTurn = FindNextTurn(this.endIndex);
            if (nextTurn.curvature * this.curvature > 0 &&
                nextTurn.GetMaxSpeed(0, null) < this.GetMaxSpeed(0, null))
            {
                if(adjust > 0)
                    adjust /= 2.5;
            }

            if (masterAdjustmentCount >= 2)
                return;

            if (masterAdjustment == 1 && adjust > 0)
                adjust /= 1.5;

            var max = GetMaxAngles();
            if (max > 54 && adjust > 0)
            {
                masterAdjustmentCount++;
                return; // do not apply if it's way too high
            }
            if (max > 25 && adjust > 0)
            {
                var f = (max - 25) / 27.0 * 0.4 + 1;
                if(adjust > 0)
                    adjust /= f;
            }
            if (max > 45 && adjust > 0)
                adjust /= 1.65;


            Console.WriteLine("Real adjustment: " + adjust);
            masterAdjustment += adjust;
            masterAdjustmentCount++;

        }

        private Turn FindNextTurn(int currentIndex)
        {
            return nextTurn;

        }

        public double GetShortCurveMultiplier(int Lane)
        {
            var len = GetLength(Lane);
            if (len > 60)
                return 1.0;
            else
            {
                var mult = 60.0 / len;
                if (mult > 1.5)
                    mult = 1.5;
                return mult;
            }
        }

        public double GetRemainingLength(int laneIndex, double elapsed)
        {
            return GetLength(laneIndex) - elapsed;
        }

        public double GetLength(int laneIndex)
        {
            var distanceOfLane = RaceInfo.GetInstance().race.track.lanes[laneIndex].distanceFromCenter;
            //Console.WriteLine("Distance of lane =" + distanceOfLane);

            // Depends on curvature of piece
            // Assumption
            // lane 0 having positive curve angle will be on outer side
            // lane 1 (or more) have positive curve angle will be on inner side
            distanceOfLane *= (curvature > 0) ? 1 : -1;
            //Console.WriteLine("New curve distance of lane =" + distanceOfLane);

            var distance = 2 * Math.PI * (this.radius - distanceOfLane) * Math.Abs(this.curvature) / 360;
            //Console.WriteLine("Curve Distance result is " + distance);
            return distance;
        }

	}

	public class TrackTurns
	{
		public Dictionary<int, Turn> IndexToTurnDictionary;
		public List<Piece> pieces;
		private Turn lastFoundTurn;

		public TrackTurns()
		{
			IndexToTurnDictionary = new Dictionary<int, Turn>();
		}

		public void LearnTrack(IEnumerable<Piece> pieces)
		{
			this.pieces = pieces.ToList();

			bool withinTurn = false; // indicator that we're still scanning inside 1 turn
			int lastScannedTurnIndex = 0;
			int countIndex = 0;
			foreach (Piece piece in pieces)
			{
				if (piece.angle == 0.0)
				{
					withinTurn = false;
					countIndex++;
					continue;
				}

				if (withinTurn == false)
				{
					CreateTurn(countIndex, piece);
					withinTurn = true;
					lastScannedTurnIndex = countIndex;
				}
				else
				{
					Turn existingTurn = IndexToTurnDictionary.Values.FirstOrDefault((v) => v.startIndex <= lastScannedTurnIndex && v.endIndex >= lastScannedTurnIndex	);
					if (existingTurn == null)
					{
						Console.WriteLine("Error: Cannot find existing turn. Index=" + lastScannedTurnIndex);
					}

					// check if we're still a same turn
					if (piece.radius == existingTurn.radius &&
						piece.angle * existingTurn.curvature > 0) // same turning side
					{
						// Add current piece to existing
						existingTurn.endIndex = countIndex;
						existingTurn.countPiece++;
						existingTurn.curvature += piece.angle;
						withinTurn = true;

					}
					else
					{
						// new Turn
						CreateTurn(countIndex, piece);
						withinTurn = true;
						lastScannedTurnIndex = countIndex;
					}
				}
				countIndex++;
			}

			foreach (var kv in IndexToTurnDictionary)
			{
                kv.Value.SetNextTurn(FindNextTurn(kv.Key));

				Console.WriteLine("Learned: ");
				Console.WriteLine("Key: " + kv.Key);
				Console.WriteLine(kv.Value.startIndex + ", " + kv.Value.endIndex + ", " + kv.Value.curvature + ", " + kv.Value.countPiece);

			}
		}



		private void CreateTurn(int index, Piece piece)
		{
			var newTurn = new Turn();
			newTurn.startIndex = index;
			newTurn.endIndex = index;
			newTurn.curvature = piece.angle;
			newTurn.countPiece = 1;
			newTurn.radius = piece.radius;

			IndexToTurnDictionary.Add(index, newTurn);
		}

		private Turn FindCurrentTurn(int currentIndex)
		{
			bool notFound = true;
			Turn foundTurn = null;

			var turns = IndexToTurnDictionary.Where(kv => kv.Key <= currentIndex);
			foreach (var kv in turns)
			{
				if (notFound == false)
					break;

				if (kv.Value.startIndex <= currentIndex && kv.Value.endIndex >= currentIndex)
				{
					foundTurn = kv.Value;
					return foundTurn;
				}
			}

			return foundTurn;
		} 

        public bool IsSafeForTurbo(int currentIndex, double turboFactor, int gameTick, double angle, double curSp)
		{
            if (Math.Abs(angle) >= 24.0)
                return false;

            var midSpeed = 15.0;
            if (curSp > 0 && curSp < 10)
            {
                midSpeed = (10 + curSp) / 2.0 + 7;
            }
			// Max straights
			// distance = gametick * 15
            var turboLengthBuffer = 25.0;
            var turboLength = gameTick * midSpeed * 1.48 * turboFactor / 3.0 + turboLengthBuffer;
            //var turboLengthBuffer = 20.0;

			bool safe = true;
			Piece[] pieces = RaceInfo.GetInstance().race.track.pieces;
			var pieceCount = RaceInfo.GetInstance().race.track.pieces.Count();
			int index = currentIndex;
			double totalCurve = 0.0;
			while (safe)
			{
				if (index > pieceCount - 1)
					index -= pieceCount;

				var thisPiece = pieces[index];
				if (thisPiece.length == 0)
				{
					totalCurve += thisPiece.angle;
					turboLength -= thisPiece.LaneLength(RaceInfo.GetInstance().raceData.currentActiveLane);
					if(totalCurve>26)
						safe = false;
					break;
				}
				else
				{
					turboLength -= thisPiece.length;
				}

				if (turboLength < 0)
					return true;

				index++;
			}

			return false;
		}

        public Turn FindPrevTurn(int currentIndex)
        {
            bool notFound = true;
            Turn prevTurn = null;

            int checkIndex = currentIndex-1;
            if (checkIndex < 0)
                return null;

            while (notFound)
            {
                var pieceCount = RaceInfo.GetInstance().race.track.pieces.Count();
                if(checkIndex > pieceCount) // TODO 1
                    checkIndex = 0;
                if (IndexToTurnDictionary.ContainsKey(checkIndex))
                {
                    notFound = false;
                    prevTurn = IndexToTurnDictionary[checkIndex];
                    lastFoundTurn = prevTurn;
                }
                else
                {
                    //Console.WriteLine("Cannot find turns");
                    //Console.WriteLine("DictLength = " + IndexToTurnDictionary.Count() + " index=" + checkIndex);
                    checkIndex--;
                    continue;
                }
            }

            return prevTurn;
        }

		private Turn FindNextTurn(int currentIndex)
		{
			bool notFound = true;
			Turn nextTurn = null;


			int checkIndex = currentIndex+1;

//			if (lastFoundTurn != null && lastFoundTurn.startIndex <= currentIndex && lastFoundTurn.endIndex >= currentIndex) // TODO 1 check index
//			{
//				nextTurn = lastFoundTurn;
//				notFound = false;
//			}

			while (notFound)
			{
				var pieceCount = RaceInfo.GetInstance().race.track.pieces.Count();
				if(checkIndex > pieceCount) // TODO 1
					checkIndex = 0;
				if (IndexToTurnDictionary.ContainsKey(checkIndex))
				{
					notFound = false;
					nextTurn = IndexToTurnDictionary[checkIndex];
					lastFoundTurn = nextTurn;
				}
				else
				{
					//Console.WriteLine("Cannot find turns");
					//Console.WriteLine("DictLength = " + IndexToTurnDictionary.Count() + " index=" + checkIndex);
					checkIndex++;
					continue;
				}
			}

			return nextTurn;

		}

		// TODO 3 Refactor this - getting too big
		public double IsSafeToNextTurn(double currentSpeed, int index, double inPiecePos, double angleSwing, double angle)
		{
			// Step 1.Find the next turn first
			var nextTurn = FindNextTurn(index);
			if (nextTurn == null)
				return 1.0;

			var currentTurn = FindCurrentTurn(index);
//			if (currentTurn == null)
//			{
//				//Console.WriteLine("------Couldn't find current turn");
//			}
			var absSwing = Math.Abs(angleSwing);
			var activeLane = RaceInfo.GetInstance().raceData.currentActiveLane;
			// This is for entering turn
			var safeDistance = nextTurn.SafeDistanceForBreak(currentSpeed);
			var phy = Physics.GetInstance();
			//Console.WriteLine("the Safe Distance is: " + safeDistance);

			// Add angle swing distance
//			if (absSwing > 3)
//			{
//				safeDistance += absSwing * 8;
//				//Console.WriteLine("new safe distance with swing is " + safeDistance + "swing " + absSwing);
//			}

            // Add existing angle into consideration to reduce speed
            var existingAngleEffect = 0.0;
            if (angle * nextTurn.curvature > 0) // positive means additive. Must reduce speed
            {
                existingAngleEffect = Math.Abs(angle) / 60 * 38; // start breaking from 30 distance - experimental
            }


			var indexDiff = nextTurn.startIndex - index;
			if (indexDiff < -10) // TODO 2 fix this
			{ 
				var trackSize = RaceInfo.GetInstance().race.track.pieces.Count();
				indexDiff += trackSize;
			}

			var diffNow = 0.0;
			var length = pieces[index].length; // Current piece length
			if (length == 0) // TODO 1 adjust for curve piece
			{
				// Current piece length
				length += pieces[index].LaneLength(activeLane);  // TODO 2 LANE

				if (nextTurn.startIndex - 1 < currentTurn.startIndex)
				{
					var trackSize = RaceInfo.GetInstance().race.track.pieces.Count();
					for (int i = index+1; i < trackSize - 1; i++)
					{
						length += pieces[i].LaneLength(activeLane); // distance in curve calculation may be consist of both straight and curve pieces.
					}

					for (int i = 0; i < nextTurn.startIndex  - 1; i++)
					{
						length += pieces[i].LaneLength(activeLane); // distance in curve calculation may be consist of both straight and curve pieces.
					}
				}
				else
				{
					for (int i = index+1; i < nextTurn.startIndex - 1; i++)
					{
						length += pieces[i].LaneLength(activeLane); // distance in curve calculation may be consist of both straight and curve pieces.
					}
				}
                diffNow = length - inPiecePos - 12;
			}
			else
			{
                diffNow = (indexDiff) * length - inPiecePos - 12; 
			}

            diffNow += existingAngleEffect;
			//Console.WriteLine("Next turn index: " + nextTurn.startIndex + ", inPiecePos: " + inPiecePos);
			//Console.WriteLine("Distance to turn is" + diffNow);

            var currentTurnLength = 0.0;
            var remainingLength = 0.0;

			if (safeDistance < diffNow)
			{
				// Step 2. Are we still inside turn?
				// This is for maintain speed inside turn
                if (currentTurn != null)
                {
                    currentTurnLength = currentTurn.GetLength(activeLane);
                    var percentage = currentTurn.GetPercentageCompletion(index, inPiecePos);
                    remainingLength = currentTurnLength * (1.0 - percentage / 100.0);
                    var curTurnMaxSpeed = currentTurn.GetMaxSpeed(activeLane, nextTurn);
                    Console.WriteLine("------Percentage = " + percentage);

                    // Since we're finishing the turn
                    var finishingMultiplier = 1.0;
                    if (remainingLength < 42.0 && remainingLength > 0.0)
                    {
                        finishingMultiplier = (42.0 - remainingLength) / 42.0 * 0.4 + 1;
                    }

                    // Step 2a. Emergency Check
                    // If swinging too strongly - break
                    if (angleSwing > 2.95 && Math.Abs(angle) > 38)
                        return 0.0;
                    if (angleSwing > 0.5 && Math.Abs(angle) > 55.0) // very very dangerous
						return 0.0;
                    if (angleSwing < 0.1 && currentSpeed < 2.5) // TOO SLOW
						return 1.0;

                    // Step 2a. Exiting Turn
                    if (percentage > 77)
                    {
                        if (Math.Abs(angle) < 54.0 && angleSwing < 0.45 && angleSwing > 0)
                        {
                            var multiplier = 1.0;
                            if (Math.Abs(angle) < 22)
                                multiplier = 1.35;
                            else if (Math.Abs(angle) < 38)
                                multiplier = 1.15;
                            else if (Math.Abs(angle) < 51)
                                multiplier = 1.03;

                            if (angleSwing < 0.1)
                                multiplier *= 1.08;

                            var targetThrottle = phy.friction * curTurnMaxSpeed * multiplier * finishingMultiplier/ phy.engine; // maintain speed - TODO 3 ensure 1.0 max

                            if (targetThrottle > 1.0)
                                return 1.0;
                            return targetThrottle;
                        }
                        if (Math.Abs(angle) < 55.0)
                        {
                            if (angleSwing < 0.5)
                                return 1.0; // maintain speed - TODO 3 ensure 1.0 max
                            else
                                return phy.friction * curTurnMaxSpeed * 0.85 / phy.engine;
                        }
                        else // greater than 55
                        {
                            if (angleSwing < -0.6)
                                return 1.0; // maintain speed - TODO 3 ensure 1.0 max
                            else if (angleSwing < 0.3)
                                return phy.friction * curTurnMaxSpeed * 0.96 / phy.engine; // maintain speed - TODO 3 ensure 1.0 max
                            else if (angleSwing > 1.6 && Math.Abs(angle) > 45.0)
                                return 0.0;
                            else
                                return phy.friction * curTurnMaxSpeed * 0.85 / phy.engine;
                        }
                       

                    }
					// Step 2b. Middle turn
					else if (percentage <= 77 && percentage > 40)
                    {
                        if (curTurnMaxSpeed > currentSpeed * 1.09 && Math.Abs(angle) < 37)
                            return 1.0; // Too slow

                        if (Math.Abs(angle) < 54 && angleSwing < 0.5 && angleSwing > 0)
                        {
                            var multiplier = 1.0;
                            //var phy = Physics.GetInstance();
                            if (Math.Abs(angle) < 22)
                                multiplier = 1.15;
                            else if (Math.Abs(angle) < 35)
                                multiplier = 1.04;
                            else if (Math.Abs(angle) < 47)
                                multiplier = 1.01;

                            if (angleSwing < 0.1)
                                multiplier *= 1.05;

                            var targetThrottle = phy.friction * curTurnMaxSpeed * multiplier  * finishingMultiplier/ phy.engine; // maintain speed - TODO 3 ensure 1.0 max

                            if (targetThrottle > 1.0)
                                return 1.0;
                            return targetThrottle;
                        } 
                        if (angleSwing < -0.55)
                            return 1.0;
                        if (angleSwing < 0)
                            return phy.friction * curTurnMaxSpeed  * finishingMultiplier / phy.engine; // maintain speed - TODO 3 ensure 1.0 max
                        else if (angleSwing > 2.5)
                        {
                            if (Math.Abs(angle) > 33)
                                return 0.0;
                            else
                                return phy.friction * curTurnMaxSpeed * 0.5 / phy.engine; 
                        }
                        else
                            return phy.friction * curTurnMaxSpeed * 0.88 / phy.engine; // maintain speed - TODO 3 ensure 1.0 max
                    }
					// Step 2c. Beginning of turn
					else
                    {
                        if (curTurnMaxSpeed < currentSpeed * 1.02)
                            return 0.0; // Too fast
                        if (curTurnMaxSpeed > currentSpeed * 1.2)
                            return 1.0; // Too slow

                        var multiplier = 1.0;
                        if (angleSwing < -1.8)
                            multiplier = 1.3;
                        else if (angleSwing < -0.5)
                            multiplier = 1.4;
                        else if (angleSwing < 2.0)
                            multiplier = 0.92;
                        else if (angleSwing < 3.0)
                            multiplier = 0.77;
                        else
                            multiplier = 0.65;

                        if (angleSwing > 0.5) // we are still increasing angle
                        {
                            if (Math.Abs(angle) > 20)
                                multiplier *= 0.90;
                            else if (Math.Abs(angle) > 36)
                                multiplier *= 0.4;
                            else if (Math.Abs(angle) > 48)
                                multiplier *= 0.0;
                        }
                        var targetThrottle = phy.friction * curTurnMaxSpeed * multiplier * finishingMultiplier/ phy.engine; // maintain speed - TODO 3 ensure 1.0 max

                        if (targetThrottle > 1.0)
                            return 1.0;

                        return targetThrottle;
                    }

//					// Try to keep nice speed inside turn
//					if (Math.Abs(angle) < 14 && angleSwing < 2.5)
//					{
//						return 1.0;
//					}
//					else if (currentTurn.GetMaxSpeed(activeLane) > currentSpeed*1.1 && Math.Abs(angle) < 38)
//					{	
//						return 1.0; // TODO: Maintain speed mode
//					}


                    // TODO exit curve mode
                    // var remainingDistance = 
                }
                else
                {
                    if (angleSwing > 0.5 && Math.Abs(angle) > 57)
                        return 0.0;
                    else if (angleSwing > 0.8 && Math.Abs(angle) > 54)
                        return 0.2;
                    else if (angleSwing > 0.3 && Math.Abs(angle) > 53)
                        return 0.8;
                    else
                        return 1.0;

                }
			}
            else if (currentSpeed < nextTurn.GetMaxSpeed(activeLane, null)) // TODO 1 Lane
			{
				var multiplier = 1.0;
				if (Math.Abs(angle) > 52)
					multiplier = 0.0;
				else if (Math.Abs(angle) > 44)
					multiplier = 0.2;
                var targetThrottle = phy.friction * nextTurn.GetMaxSpeed(activeLane, null) * multiplier/ phy.engine; // maintain speed - TODO 3 ensure 1.0 max

				return targetThrottle;
			}
			else
				return 0.0;
		}

		public void RecordAngle(CarPosition position, double speed)
		{
			var turn = FindCurrentTurn(position.piecePosition.pieceIndex);
			if (turn == null)
				return;
			turn.RecordMaxAngle(position.piecePosition.pieceIndex, position.angle, speed);
		}

        public void JustCrashed(int index)
        {
            try
            {
                foreach (var turns in IndexToTurnDictionary.Values)
                {
                    turns.JustCrashedReduceGlobal();
                }

                var prevturn = FindPrevTurn(index);
                if (prevturn != null)
                {
                    Console.WriteLine("Adjusted prev turn");
                    prevturn.JustCrashed();
                }

                var turn = FindCurrentTurn(index);
                if (turn == null)
                    return;
                turn.JustCrashed();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Exception when adjusting crash" + ex);
            }
        }

		public void AnalyseLap()
		{
            var sumMaxAngle = new List<double>();
            var aveMaxAngle = 0.0;
			foreach (var turn in IndexToTurnDictionary)
			{
				turn.Value.AnalyseMaxAngle();
                sumMaxAngle.Add(turn.Value.GetMaxAngles());
			}

            sumMaxAngle.Sort();
            // take top 3
            int maxcount = 0;
            if(sumMaxAngle.Count() > 2)
            {
                while (maxcount < 2)
                {
                    aveMaxAngle += sumMaxAngle[sumMaxAngle.Count - maxcount - 1];
                    maxcount++;
                }

            }
            else
            {
                while (maxcount < sumMaxAngle.Count())
                {
                    aveMaxAngle += sumMaxAngle[maxcount];
                    maxcount++;
                }
            }
            aveMaxAngle /= maxcount;

            var aveSlippage = 0.0;
            int count = 0;
			foreach (var turn in IndexToTurnDictionary)
			{
                var slip = turn.Value.AnalyseSlippage();
                if (turn.Value.SlippageQualified())
                {
                    aveSlippage += Math.Abs(slip);
                    count++;
                }
			}
            if(count > 0)
                aveSlippage /= count;


            var masterAdjustment = 0.0; // Additive speed
            var frictionGrip = 1.0;
            var friction = Physics.GetInstance().friction;
            if (friction > 0.022)
                frictionGrip *= 1.01;
            else if(friction < 0.0186)
                 friction *= 0.98;
            // Overall Learning
            // Too slow
            if (aveSlippage < 1.9 && aveMaxAngle > 0 && Math.Abs(aveMaxAngle) <= 44)
            {
                var masterAdjustmentFromSlip = 1.5 - ((1.9 - Math.Abs(aveSlippage)) / 1.9 * 1.5);
                var masterAdjustmentFromAngle = (44.0-Math.Abs(aveMaxAngle)) / 44.0 * 0.5;
                masterAdjustment = (masterAdjustmentFromAngle*1.4 + masterAdjustmentFromSlip) / 2.4;
                Console.WriteLine("Master adjustment due to overall slow slip: " + masterAdjustment + " maxAngle " + aveMaxAngle);
            }

            // Too fast
            if (aveSlippage >= 2.1 && aveMaxAngle > 0 && Math.Abs(aveMaxAngle) < 60.0)
            {
                masterAdjustment = (2.1 - Math.Abs(aveSlippage)) / 2.1 * 0.8;
                Console.WriteLine("Master adjustment due to higher slip: " + masterAdjustment + " maxAngle " + aveMaxAngle);
            }
            if(Math.Abs(aveMaxAngle) > 55.0)
            {
                masterAdjustment = -0.13;
            }


            // Reset our slippage calculation
            foreach (var turn in IndexToTurnDictionary)
            {
                turn.Value.ApplyMasterAdjustment(masterAdjustment * frictionGrip);
                turn.Value.ResetSlippage();
            }

		}

        public void ResetMaxSpeed()
        {
            foreach(Turn turn in IndexToTurnDictionary.Values)
            {
                turn.ResetSpeed();
            }
        }

	}

	public class Physics
	{
		private static Physics instance;

		public const double maxAngle = 45;
		public const double maxRisk = 4.0;

		public double friction;
		public double mass;
		public double engine;
		public double throttleForLearning;
		// engine*throttle - friction*speed = mass*acceleration

		public List<double> speedList;
		public List<double> accelList;

		public int countLearned;
		public bool learnCompleted;

		private Physics()
		{
			speedList = new List<double> ();
			accelList = new List<double> ();
			learnCompleted = false;
			mass = 1.0;
			throttleForLearning = 1.0;
		}

		public static Physics GetInstance()
		{
			if (instance == null)
			{
				instance = new Physics();
			}

			return instance;
		}

		public void Learn(double speed)
		{
			if (learnCompleted)
				return;

			if (speed != 0 )
			{
				if (speedList.Count > 0)
				{
					var prevSpeed = speedList.Last();
					accelList.Add(speed - prevSpeed);
				}
				speedList.Add(speed);
				countLearned++;

				if (countLearned >= 3)
					CalculatePhysics();
			}
		}

		private bool CheckLearnCompleted()
		{
			return learnCompleted;
		}

		private void CalculatePhysics()
		{
			if (countLearned < 3)
				return;

			try
			{
				engine = (accelList[0] * speedList[1] - accelList[1] * speedList[0]) /
				         ((speedList[1] - speedList[0]) * throttleForLearning);

				friction = (engine * throttleForLearning - mass * accelList[1]) / speedList[1];
				learnCompleted = true;
				Console.WriteLine("Physics calc completed. friction = " + friction + " engine = " + engine);
			}
			catch(DivideByZeroException)
			{
				// learn again! most likely invalid data
				// TODO - add invalid offset to learn again
				Console.WriteLine("Divide by zero exception while learning");
			}
		}

		public double MaxDeceleration(double curSpeed)
		{
			// TODO: Wrong here
			return friction * curSpeed;
		}
	}


//	public class TrackSwitches
//	{
//		public Dictionary<int, SwitchPiece> switches;
//
//
//		public TrackSwitches()
//		{
//			switches = new Dictionary<int, @SwitchPiece>();
//		}
//
//		public void LearnTrack(IEnumerable<Piece> pieces)
//		{
//			int index = 0;
//			foreach (Piece piece in pieces)
//			{
//				if (piece.@switch == true)
//				{
//					switches.Add(index, new SwitchPiece());
//				}
//				index++;
//			}
//		}
//	}
//


	public class SpeedRecording
	{
		public Stack<double> LastSpeed;
		private int count;
		private	double sum;
		private const int StackLength = 20;

		public SpeedRecording()
		{
			LastSpeed = new Stack<double>();
			sum = 0;
		}

		public void Record(double speed)
		{
			LastSpeed.Push(speed);
			sum += speed;
			count++;

			if (StackLength >= 20)
			{
				double popValue = LastSpeed.Pop();
				sum -= speed;
				count--;
			}
		}

		public double GetAverage()
		{
			return sum/count;
		}
	}

}
