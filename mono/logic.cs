using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace helloworld
{


	public class SwitchAnalysis
	{
		public Dictionary<int, SwitchPiece> switchPieces; // int = index
		public Dictionary<int, double> sumAngle; // until the next switch lane
		public bool SwitchPending;
		public bool SwitchSent;
		public string SwitchDirection;
        private int nearSwitchCounter;

		public SwitchAnalysis()
		{
			switchPieces = new Dictionary<int, SwitchPiece>(); // key is index location of switch
			sumAngle = new Dictionary<int, double>();
			SwitchPending = false;
            nearSwitchCounter = 0;
		}

		public bool CheckSwitchedLane(CarPosition carPos)
		{
			var lane = carPos.piecePosition.lane;
			//Console.WriteLine("Old Lane = " + lane.startLaneIndex + " NewLane=" + lane.endLaneIndex);
			if (RaceInfo.GetInstance().raceData.currentActiveLane != lane.endLaneIndex)
			{
				RaceInfo.GetInstance().raceData.currentActiveLane = lane.endLaneIndex;

				SwitchPending = false; // we have just switched
                nearSwitchCounter = 0;
				return true;
			}

			return false;
		}

		public bool NearSwitch(int index)
		{
			var nextIndex = FindNextSwitchPiece(index);
            if (nextIndex == index + 1 ||
            (nextIndex == 0 && RaceInfo.GetInstance().race.track.pieces.Count() == index + 1))
            {
                if (nearSwitchCounter < 3)
                {
                    nearSwitchCounter++;
                    return false;
                }
                else
                {
                    nearSwitchCounter = 0;
                    return true;
                }
            }
			return false;
		}

		public string GetSwitchToShortestRoute(int index, int currentLane)
		{
			var nextIndex = FindNextSwitchPiece(index);
			var piece = switchPieces[nextIndex];
			var angle = sumAngle[nextIndex];

			if (angle == 0)
				return string.Empty;

			if (angle > 0) // TODO switch based on lane count
			{
				if (CheckIfStillCanSwitch(currentLane, "Right"))
					return "Right";
				else
					return string.Empty;
			}
			else 
			{
				if (CheckIfStillCanSwitch(currentLane, "Left"))
					return "Left";
				else
					return string.Empty;
			}
		}

        public string GetSwitchToOtherRoute(int index, int currentLane)
        {
            var nextIndex = FindNextSwitchPiece(index);
            var piece = switchPieces[nextIndex];
            var angle = sumAngle[nextIndex];

            if (angle == 0)
                return string.Empty;

            if (angle < 0) // TODO switch based on lane count
            {
                if (CheckIfStillCanSwitch(currentLane, "Right"))
                    return "Right";
                else
                    return string.Empty;
            }
            else 
            {
                if (CheckIfStillCanSwitch(currentLane, "Left"))
                    return "Left";
                else
                    return string.Empty;
            }
        }

		public bool CheckIfStillCanSwitch(int lane, string switchside)
		{
			// Determine our side (where is lane 0...)
			var laneCount = RaceInfo.GetInstance().track.lanes.Count();
			var laneDistance0 = RaceInfo.GetInstance().track.lanes[0].distanceFromCenter;
			var laneToTheLeft = 0;
			var laneToTheRight = 0;

			if (laneDistance0 < 0) // Means lane 0 on top. Switch right means increase lane index
			{
				laneToTheLeft = lane;
				laneToTheRight = laneCount - lane -1;
			}
			else
			{
				laneToTheLeft = laneCount - lane -1;
				laneToTheRight = lane;
			}

			if (switchside == "Right" && laneToTheRight > 0)
				return true;
			if (switchside == "Left" && laneToTheLeft > 0)
				return true;

			return false;
		}

		public void Analyse(IEnumerable<Piece> pieces)
		{
			int index = 0;
			var numLane = RaceInfo.GetInstance().race.track.lanes.Count();
			foreach(Piece piece in pieces)
			{
				if (piece.@switch == true)
				{
					switchPieces.Add(index, new SwitchPiece(index, numLane)); // TODO 3 update number of lane
				}

				index++;
			}


			foreach (var switchPieceIndex in switchPieces.Keys)
			{
				//Sum of angle between this piece to next piece
				var thisPieceIndex = switchPieceIndex;
				var nextPieceIndex = FindNextSwitchPiece(thisPieceIndex);

				sumAngle.Add(thisPieceIndex, CalculateAngleInBetween(thisPieceIndex, nextPieceIndex));
			}
		}

		public int FindNextSwitchPiece(int index)
		{
			var nextIndex = switchPieces.Keys.Where(p => p > index);
			if (nextIndex.Count() == 0)
			{
				// Current index is already the max
				return switchPieces.Keys.Min();

			}
			return nextIndex.Min();
		}

		public double CalculateAngleInBetween(int fromIndex, int toIndex)
		{
			if (fromIndex == toIndex)
				return 0;

			var startIndexToCount = fromIndex;
			var endIndexToCount = toIndex;
			if (fromIndex > toIndex)
				endIndexToCount = RaceInfo.GetInstance().race.track.pieces.Count()-1;

			var pieces = RaceInfo.GetInstance().race.track.pieces;
			var sum = 0.0;
			for (int i = startIndexToCount + 1; i < endIndexToCount; i++)
			{
				sum += pieces[i].angle;
			}

			if (fromIndex > toIndex)
			{
				for (int i = 0; i < toIndex; i++)
				{
					sum += pieces[i].angle;
				}
			}

			sum += pieces[fromIndex].angle / 2; // Assume switch angle is in the middle
			sum += pieces[toIndex].angle / 2;

			return sum;
		}
	}


//	public class CarAnalysisService
//	{
//		Dictionary<CarId, CarAnalysis> carAnalysisDict;
//		public CarId myCarId;
//
//		public CarAnalysisService()
//		{
//			// TODO 1
//			carAnalysisDict = new Dictionary<CarId, CarAnalysis>();
//		}
//
//		public void Initialize(IEnumerable<Car> cars, IEnumerable<Piece> pieces)
//		{
//			// TODO 0.2
//			// Initialize car and caranalysis
//
//			// Initialize the checkpoints
//		}
//	}
//
//	public class CarAnalysis
//	{
//		public SpeedRecording speedRecording;
//		public SpeedCalculation speedCalculation;
//		public CheckPoint checkPoint;
//
//		public void UpdatePosition(CarPosition newPosition)
//		{
//			// TODO 1
//
//		}
//
//		public void TakeSnapshot(IEnumerable<CarPosition> carPositions)
//		{
//
//		}
//
//
//	}
//
//	public class CheckPoint
//	{
//		public int indexLocation;
//		public Dictionary<CarId, int> carTime;
//
//		public bool IsCheckPoint(int index)
//		{
//			return false; // todo 1
//		}
//	}

	public class OvertakeLogic
	{
		
		private List<SwitchPiece> switchPieces;

		public bool turboAvailable;
		public int laneCount;
        public List<CarPosition> positions;
        public        string myCarName;
        public OvertakeLogic(string myCarName)
		{
            this.myCarName = myCarName;
            //carAnalysisService = new CarAnalysisService();
            positions = new List<CarPosition>();
		}

		// TODO 0.1
		public void Initialize(Race race)
		{
			// Initialize track pieces - check where are the switches
			laneCount = race.track.lanes.Count();

            positions = new List<CarPosition>();

			// State self
			//carAnalysisService.myCarId = ?? // TODO 0.3
		}

		public void UpdateState(IEnumerable<CarPosition> carpositions)
		{
            positions.Clear();
            foreach (var pos in carpositions)
            {
                positions.Add(pos);
            }
		}

        public bool DetermineSwitch()
        {
            try
            {
                Dictionary<CarPosition, double> distanceToCar = new Dictionary<CarPosition, double>();

                foreach (var car in positions)
                {
                    if (car.id.name == myCarName)
                        continue;
                    distanceToCar.Add(car, FindDistance(GetMyCarPosition(), car));
                }

                var ordered = distanceToCar.OrderBy(a => a.Value).FirstOrDefault();
                if (ordered.Key == null)
                    return false;
                
                Console.WriteLine("Nearest distance to other car: " + ordered.Value);

                if (ordered.Value < 130 &&
                   ordered.Key.piecePosition.lane.endLaneIndex == GetMyCarPosition().piecePosition.lane.endLaneIndex)
                {
                    return true;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error in switch " + ex);
            }
             

            return false;

        }

       

        private CarPosition GetMyCarPosition()
        {
            var carPos = positions.First(cp => cp.id.name == myCarName);

            return carPos;
        }

        // positive value means we are in front
        public double FindDistance(CarPosition myCar, CarPosition otherCar)
        {
            var indexMyCar = myCar.piecePosition.pieceIndex;
            var indexOtherCar = otherCar.piecePosition.pieceIndex;

            int diffIndex = indexMyCar - indexOtherCar;
            if (diffIndex > 0)
                return 500; // Don't care we are in front

            double distanceTotal = 0.0;
            if (diffIndex >= -3)
            {
                var raceInfo = RaceInfo.GetInstance();
                var raceData = raceInfo.raceData;
               
                var myLane = myCar.piecePosition.lane.endLaneIndex;
                var otherLane = otherCar.piecePosition.lane.endLaneIndex;

                // my piece remaining
                var myCarPiece = raceInfo.race.track.pieces[indexMyCar];
                distanceTotal += myCarPiece.RemainingDistance(myCar.piecePosition.inPieceDistance);

                // other piece - must have extra gap of at least 1 piece
                var tempIndex = indexMyCar + 1;
                while (tempIndex < indexOtherCar)
                {
                    var piece = raceInfo.race.track.pieces[tempIndex];
                    distanceTotal += piece.LaneLength(myLane);
                    tempIndex++;
                }

                if (diffIndex == 0)
                {
                    distanceTotal = otherCar.piecePosition.inPieceDistance - myCar.piecePosition.inPieceDistance;
                }
                // his piece elapsed
                else
                {
                    distanceTotal += otherCar.piecePosition.inPieceDistance;
                }
                return distanceTotal;
            }

            return 500;

        }

		public void AnalyseBots()
		{
			// todo 0
		}

		public int GetTargetLane()
		{
			return 0; // todo 1
		}

		public bool SafeToTurbo()
		{
			return false; // todo 2
		}

		public List<int> PrioritizeLane()
		{
			return null; // todo 2
		}
	}

	// Learn the slippage rate
	public class Slippage
	{
		public double slippageConst; // angle drifting per tick @max speed
		public Dictionary<int, double> angleList;
		public Dictionary<int, double> speedList;
		private int counter;
		private const int maxCounter = 12;
		public double calculatedMaxSpeed;
		public double entryAngle;

		// Experimental value
		double lowSpeedConst = 3.2;
		double lowSpeedLimit;

		public Slippage()
		{
			angleList = new	Dictionary<int, double>();
			speedList = new Dictionary<int, double>();
			counter = 0;
			calculatedMaxSpeed = 0;
			entryAngle = 0;
		}

		public void Reset()
		{
			counter = 0;
            calculatedMaxSpeed = 0;
            entryAngle = 0;
			angleList.Clear();
			speedList.Clear();

		}

		public double AverageSpeedPercentage(int fromIndex, int toIndex)
		{
			var sum = 0.0;
			if (fromIndex < 1)
				fromIndex = 1;
			if(toIndex > speedList.Count() - 1)
				toIndex = speedList.Count()  - 1;

			int count = 0;

			for (int i = fromIndex; i < toIndex; i++)
			{
				var sp = speedList[i];
				count++;
				sum += sp;
			}

			return sum / count / calculatedMaxSpeed * 100;
		}

		public void Record(double angle, double speed, double maxSpeed)
		{
			if (counter > maxCounter)
				return;

			if (calculatedMaxSpeed == 0)
			{
				calculatedMaxSpeed = maxSpeed;
				entryAngle = angle;
			}

			if (calculatedMaxSpeed != maxSpeed)
				return; // something went wrong

			angleList.Add(counter, angle);
			speedList.Add(counter, speed);
			counter++;
		}
		
		public double AnalyseSlippage(int fromIndex, int toIndex)
		{
			if (fromIndex < 1)
				fromIndex = 1;
			if(toIndex > angleList.Count() - 1)
				toIndex = angleList.Count()  - 1;

			var listSlip = new List<double>();
			double sum = 0.0;
			for (int i = fromIndex; i < toIndex; i++)
			{
				var slip = angleList[i] - angleList[i - 1];
				var normalized = NormalizeAngleSlip(slip, speedList[i]);
				listSlip.Add(normalized);
				sum += normalized;
			}

			return sum / listSlip.Count();
		}

		public double InitialSwing()
		{
			return AnalyseSlippage(1, 2);
		}

		private double NormalizeAngleSlip(double angleSlip, double speed)
		{
			//if(speed > calculatedMaxSpeed)

			var diff = calculatedMaxSpeed - speed;
			if (diff > 0.7)
				diff = 0.7;

			return angleSlip * (1 + lowSpeedConst * diff / 0.7);
		}

		public bool IsQualified()
		{
			// 1. High enough speed 
			bool highSpeed = AverageSpeedPercentage(2, 6) > 85.0 ? true : false;
			// 2. enough data
			bool enoughData = angleList.Count() >= 7 ? true : false;
			// 3. low entry and low start slip
            bool lowEntry = Math.Abs(entryAngle) < 3.9 && Math.Abs(AnalyseSlippage(1, 2)) < 0.88 ? true : false;

			return highSpeed && enoughData && lowEntry;
		}
	}
}
