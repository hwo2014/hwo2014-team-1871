
using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;


namespace helloworld
{
	public class MsgWrapper {
		public string msgType;
		public Object data;


		public MsgWrapper(string msgType, Object data) {
			this.msgType = msgType;
			this.data = data;

		}

	}

	public class MsgWrapperRecv {
		public string msgType;
		public Object data;
		public string gameId;
		public int gameTick;
	}


	abstract class SendMsg {
		public string ToJson() {
			var st = JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));

			if (this.MsgType() == "joinRace")
				Console.WriteLine(st);
			return st;
		}
		protected virtual Object MsgData() {
			return this;
		}

		protected abstract string MsgType();

	}

	class CreateNewRace: SendMsg {
		public string name;
		public string key;
        //public string color;
		public string trackName;
		public string password;
		public int carCount;

		public CreateNewRace(string name, string key) {
			this.name = name;
			this.key = key;
            //this.color = "red";
			this.trackName = string.Empty;
		}

		public CreateNewRace(string name, string key, string trackName, int carCount) {
			this.name = name;
			this.key = key;
            //this.color = "red";
			this.trackName = trackName;
			password = "";
			this.carCount = carCount;
		}


		protected override string MsgType() { 
			return "create";
		}
	}



	class JoinRace: SendMsg {
		public BotId botId;
		//	public string color;
		public string trackName;
		public string password;
		public int carCount;

		public JoinRace(string name, string key) {
			this.botId = new BotId(name, key);
			//this.color = "red";
			this.trackName = string.Empty;
			carCount = 1;
		}

		public JoinRace(string name, string key, string trackName) {
			this.botId = new BotId(name, key);
			//this.color = "red";
			this.trackName = trackName;
			password = "";
			carCount = 1;
		}

		public JoinRace(string name, string key, string trackName, int carCount) {
			this.botId = new BotId(name, key);
			//this.color = "red";
			this.trackName = trackName;
			password = "";
			this.carCount = carCount;
		}

        public JoinRace(string name, string key, string trackName, int carCount, string pwd) {
            this.botId = new BotId(name, key);
            //this.color = "red";
            this.trackName = trackName;
            password = pwd;
            this.carCount = carCount;
        }

		protected override string MsgType() { 
			return "joinRace"; 
		}
	}

	class Join: SendMsg {
		public string name;
		public string key;
		//public string color;

		public Join(string name, string key) {
			this.name = name;
			this.key = key;
			//this.color = "red";

		}

		protected override string MsgType() { 
			return "join"; 
		}
	}

	class Ping: SendMsg {
		protected override string MsgType() {
			return "ping";
		}
	}

	class Turbo: SendMsg{
		protected override string MsgType() {
			return "turbo";
		}

		protected override Object MsgData() {
			return "Weeeeeee";
		}

	}

	class SwitchLane: SendMsg
	{
		// TODO 2 switch back
		public string direction;

		public SwitchLane(string direction)
		{
			this.direction = direction;
		}

		protected override string MsgType() 
		{
			return "switchLane";
		}

		protected override Object MsgData() 
		{
			return direction;
		}

	}

	class Throttle: SendMsg {
		public double value;

		public Throttle(double value) {
			this.value = value;
		}

		protected override Object MsgData() {
			return this.value;
		}

		protected override string MsgType() {
			return "throttle";
		}
	}
}